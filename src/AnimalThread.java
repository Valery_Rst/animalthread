package net.rstvvoli.animalThreads;

public class AnimalThread extends Thread {
    AnimalThread(String name, int priority) {
        this.setName(name);
        this.setPriority(priority);
    }

    @Override
    public void run() {

        System.out.printf("%s побежал(а)... Приоритет: %d. \n", getName(), getPriority());
        for (int metres = 0; metres <= 50000; metres++) {

            if (metres % 1000 == 0) {
                System.out.printf("%s прошел(а) %d метров. \n", getName(), metres);
            }

            if (metres == 25000 & getPriority() == MAX_PRIORITY) {
                System.out.printf("%s сменил(а) приоритет... Приоритет: %s \n", getName(), getPriority());
                setPriority(MIN_PRIORITY);
            }
            else if (metres == 25000 & getPriority() == MIN_PRIORITY) {
                System.out.printf("%s сменил(а) приоритет... Приоритет: %s \n", getName(), getPriority());
                setPriority(MAX_PRIORITY);
            }

            if (metres == 50000) {
                System.out.printf("%s финишировал(а)... Приоритет: %d. \n", getName(), getPriority());
            }
        }
    }
}