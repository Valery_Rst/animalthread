package net.rstvvoli.animalThreads;

public class RabbitAndTurtle {
    public static void main(String[] args) {

        AnimalThread rabbit = new AnimalThread("Кролик", Thread.MAX_PRIORITY);
        AnimalThread turtle = new AnimalThread("Черепаха", Thread.MIN_PRIORITY);

        rabbit.start();
        turtle.start();

        System.out.println("Забег начат!");
    }
}